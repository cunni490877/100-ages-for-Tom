#include <iostream>
using namespace std;
// To support Windows Systems
#include <bits/stdc++.h>

int main () {
   // local variable declaration:
    string characterName = "Tom";
    int characterAge;
    characterAge = 1;
   // check the boolean condition
   LOOP:do {
      if( characterAge < 100 ) {
         // if condition is true then print the following
         cout << "There once was a man named " << characterName << "." << endl;
         cout << "He turned " << characterAge << " years old" << "." << endl;
         cout << "He liked the name " << characterName << "." << endl;
         cout << "But did not like being " << characterAge << "." << endl;
         system("read");
         cout << "_______________________________" << endl;
         cout << "                               " << endl;
         characterAge = characterAge + 1;
         goto LOOP;

      } else {
         // if condition is false then print the following
         cout << "There once was a man named " << characterName << "." << endl;
         cout << "He turned " << characterAge << " years old" << "." << endl;
         cout << "He liked the name " << characterName << "," << endl;
         cout << "And did like being " << characterAge << "." << endl;
         return 0;
      }

   while( characterAge > 100 );
   return characterAge;
;}
while( characterAge > 100 );
}
